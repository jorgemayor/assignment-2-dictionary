section .data
newline: db 10

section .text

GLOBAL exit
GLOBAL string_length
GLOBAL print_string
GLOBAL print_char
GLOBAL print_newline
GLOBAL print_uint
GLOBAL print_int
GLOBAL string_equals
GLOBAL read_char
GLOBAL read_word
GLOBAL parse_uint
GLOBAL parse_int
GLOBAL string_copy

; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
	xor rdi, rdi
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	mov rax, 0
		.loop:
		xor rax, rax
	.count:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .count
	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	mov  rdx, rax
	mov  rsi, rdi
	mov  rax, 1
	mov  rdi, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	mov rsi, rsp
	dec rsi
	mov byte[rsi], dil
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rax, 1
	mov rdi, 1
	mov rsi, newline
	mov rdx, 1
	syscall
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
		mov rax, rdi
		mov r8, 10
		mov r9, rsp
		dec rsp
	.division:
		mov rdx, 0
		div r8
		add rdx, 0x30
	.forStack:
		dec rsp
		mov byte[rsp], dl
		cmp rax, 0
		jnz .division
	.print:
		mov rdi, rsp
		call print_string
		mov rsp, r9
		ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
		mov rdx, 0
		cmp rdi, rdx
		js .negative
		jmp .positive
	.negative:	
		push rdi
		mov rdi, '-'
		call print_char
		pop rdi
		neg rdi
	.positive:
		call print_uint
		ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
		xor rax, rax
		mov al, [rdi]
		cmp al, [rsi]
		jnz .finish
		inc rsi
		add rdi, 1
		test al, al
		jnz string_equals
		mov rax, 1
		ret
	.finish:
		xor rax, rax
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	mov rsi, rsp
	mov rdx, 1
	xor rdi, rdi
	xor rax, rax
	syscall
	pop rax
 	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
		mov r8, rdi
		mov r9, rsi
		mov r10, rdi
	.repeat:
		call read_char
		cmp rax, 0xA
		je .repeat
		cmp rax, 0x9
		je .repeat
		cmp rax, 0x20
		je .repeat
	.read:
		cmp rax, 0x0
		je .finish
		cmp rax, 0xA
		je .finish
		cmp rax, 0x9
		je .finish
		cmp rax, 0x20
		je .finish
		dec r9
		cmp r9, 0
		je .stop
		mov byte[r8], al
		inc r8
		call read_char
		jmp .read
	.stop:
		xor rax, rax
		ret
	.finish:
		mov byte[r8], 0
		mov rdx, r8
		sub rdx, r10
		mov rax, r10
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
		mov rdx, 0
		mov rcx, rax
		mov rax, 0
		mov rsi, rdi
		mov r8, 10	
	.repeat:
		mov rdi, 0
		mov dil, byte[rsi+rdx]
		cmp dil, '0'
		jb .finish
		cmp dil, '9'
		ja .finish
		sub dil, '0'
		imul rax, r8
		add rax, rdi
		inc rdx
		dec rcx
		jne .repeat
	.finish:
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
		xor r8, r8
	.work:
		mov al, byte[rdi]
		cmp al, "-"
		jne parse_uint
		inc rdi
		call parse_uint
		cmp rdx, r8
		je .finish
		inc rdx
		neg rax
	.finish:
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	.loop:
		call string_length
		inc rax
		cmp rdx, 0
		je .finish
		cmp rax, rdx
		jg .onMinus
		mov al, [rdi]
		mov [rsi], al
		cmp byte[rdi], 0
		je .copy
		inc rsi
		inc rdi
		dec rdx
		jmp .loop
	.copy:
		mov rax, rsi
	.onMinus:
		xor rax, rax
		ret	
	.finish:
		xor rax, rax
		ret

